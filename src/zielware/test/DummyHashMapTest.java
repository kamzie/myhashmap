/**
 * @author Kamil Zielinski
 * @date: 23-07-2018
 */
package zielware.test;

import zielware.wordpress.com.DummyHashMap;
import static org.junit.jupiter.api.Assertions.*;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.jupiter.api.*;

class DummyHashMapTest {

	protected static final Logger LOGGER = Logger.getLogger(DummyHashMap.class.getName());
	DummyHashMap hm;
	
	//execute only once, in the starting
	@BeforeEach
    @DisplayName("@BeforeEach")
    void createNewDummyHashMap() {
    	hm = new DummyHashMap();
		hm.put("1", "pierwszy");
		System.out.println("BeforeAll");
		LOGGER.log(Level.INFO, "BeforeAll");
    }
    
	@Test
	@DisplayName("Constructor test")
	void testDummyHashMap() {		
		assertTrue(hm!=null);
		System.out.println("getCurrentSize:" +  hm.getCurrentSize());
		assertEquals(16, hm.getCurrentSize());
	}

	@Test
	@DisplayName("Put and Get")
	void testPutAndGet() {
		hm.put("2", "drugi");
		assertEquals(2, hm.getCurrentNoElements());
		assertTrue(hm.get("2").equals("drugi"));
	}
	
	@Test
	@DisplayName("Contains Key")
	void testContainsKey() {
		assertTrue(hm.containsKey("1")==true);
	}

	@Test
	@DisplayName("Contains Value")
	void testContainsValue() {
		assertTrue(hm.containsValue("pierwszy")==true);
	}

	@Test
	@DisplayName("Increase HashMap")
	void testIncreaseHashmap() {
		hm.put("2", "asd");
		hm.put("3", "zxcxzc");
		hm.put("4", "ewrewr");
		hm.put("5", "pierwszy");
		hm.put("6", "pierwszy");
		hm.put("7", "sdfsdf");
		hm.put("8", "pierwszy");
		hm.put("9", "pierwszy");
		hm.put("111", "pierwszy");
		hm.put("2122", "eryery");
		hm.put("3223", "pierwszy");
		hm.put("4334", "wrewr");
		hm.put("5445", "adsf");
		hm.put("64455", "adsf");	
		hm.put("74456", "adsf");	
		hm.put("81Z4457", "adsf");	
		hm.put("914458", "adsf");	
		hm.put("014459", "adsf");	
		hm.put("94459", "adsf");
		hm.put("kkkasd", "adsf");
		hm.put("k1234kzzzkasd", "adsf");
		assertTrue(hm.getCurrentSize()>16);
	}

	@Test
	@DisplayName("Clone HashMap")
	void testCloneDummyHashMap() {
		DummyHashMap hm2 = hm.clone();
		assertTrue(hm.equals(hm2));
	}

	@Test
	@DisplayName("Is Empty")
	void testIsEmpty() {
		assertTrue(hm.getCurrentNoElements()>0);
	}

	@Test
	@DisplayName("Merge")	
	void testMerge() {
		
		DummyHashMap hm3 = new DummyHashMap();
		hm3.put(1,111);
		try {
			DummyHashMap.merge(hm, hm3);
		} catch(Exception e) {
			System.out.println("myyk exceptionem");
			assertTrue(true);
		}
	}
	
	@Test
	@DisplayName("Clear")	
	void testClear() {
		assertTrue(hm!=null);
		assertEquals(16, hm.getCurrentSize());
	}
}