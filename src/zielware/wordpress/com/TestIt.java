/**
 * @author Kamil Zielinski
 * @date: 23-07-2018
 *
 */
package zielware.wordpress.com;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestIt {
	
	//java.util.logging.Loger
	protected static final Logger LOGGER = Logger.getLogger(TestIt.class.getName()); 
	
	public static void main(String[] args) {
	
		DummyHashMap dHashMap = new DummyHashMap();
		
		dHashMap.put("1", "pierwszy");
		dHashMap.put("2", "asd");
		dHashMap.put("3", "zxcxzc");
		dHashMap.put("4", "ewrewr");
		dHashMap.put("5", "pierwszy");
		dHashMap.put("6", "pierwszy");
		dHashMap.put("7", "sdfsdf");
		dHashMap.put("8", "pierwszy");
		dHashMap.put("9", "pierwszy");
		dHashMap.put("111", "pierwszy");
		dHashMap.put("2122", "eryery");
		dHashMap.put("3223", "pierwszy");
		dHashMap.put("4334", "wrewr");
		dHashMap.put("5445", "adsf");
		dHashMap.put("64455", "adsf");	
		dHashMap.put("74456", "adsf");	
		dHashMap.put("81Z4457", "adsf");	
		dHashMap.put("914458", "adsf");	
		dHashMap.put("014459", "adsf");	
		dHashMap.put("94459", "adsf");
		dHashMap.put("kkkasd", "adsf");
		dHashMap.put("k1234kzzzkasd", "adsf");
	
		DummyHashMap dHashMap2 = new DummyHashMap();
		dHashMap.put("kamilz1", "eryery");
		dHashMap2.put("kamilz12", "pierwszy");
		dHashMap2.put("kamilz13", "wrewr");
		dHashMap2.put("kamilz14", "adsf");
		dHashMap2.put("kamilz15", "adsf");
		
		DummyHashMap result = new DummyHashMap();
		result = DummyHashMap.merge(dHashMap, dHashMap2);
		result.printCollection();

		
	}
}
