/*
 * @author Kamil Zielinski
 * https://zielware.wordpress.com/
 * Created: 19-07-2018
 * 
 * Idea was to creating simple class which provides functionality
 * similar to the HashMap.
 * It is first version, next should be more compatible with the official HashMap 
 */
package zielware.wordpress.com;

import java.io.Serializable;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DummyHashMap implements Serializable, Cloneable {

	private static final long serialVersionUID = -7085975134455751751L;

	protected static final Logger LOGGER = Logger.getLogger(DummyHashMap.class.getName());

	private Node[] nodeArray;
	// capacity is the number of buckets - by default 16
	private final int DEFAULT_INITIAL_CAPACITY = 1 << 4;
	private int current_no_elements;

	// load_factor is used to perform calculations and guess if automatic increase
	// is required
	private final float load_factor = 0.75f;
	// current_size should be increment when current_no_elements x load_factor becomes large
	private int current_size;

	public class Node {

		private Object key;
		private Object value;

		Node(Object k, Object v) {
			key = k;
			value = v;
		}

		public Object getKey() {
			return key;
		}

		public Object getValue() {
			return value;
		}

		private DummyHashMap getOuterType() {
			return DummyHashMap.this;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((key == null) ? 0 : key.hashCode());
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Node other = (Node) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (key == null) {
				if (other.key != null)
					return false;
			} else if (!key.equals(other.key))
				return false;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}
	}

	// c-tor
	public DummyHashMap() {
		nodeArray = new Node[DEFAULT_INITIAL_CAPACITY];
		current_size = DEFAULT_INITIAL_CAPACITY;
		current_no_elements = 0;
	}

	public void put(Object k, Object v) {
		// capactiy is automatically increased when:
		if (current_no_elements >= (current_size * load_factor)) {
			increaseHashmap();
		}
		// calculate position to insert the new element
		int position = calculatePosition(k, v);

		Node element = new Node(k, v);
		// If position is filled, new element overwrite it without increasing number of
		// elements in array
		if (nodeArray[position] == null) {
			// increment elements no
			LOGGER.log(Level.INFO,
					"nodeArray[" + position + "] == null, key: " + k + " current_no_elements: " + current_no_elements);
			current_no_elements++;
		} else {
			LOGGER.log(Level.INFO,
					"nodeArray[" + position + "] != null, key: " + k + " current_no_elements: " + current_no_elements);
		}

		nodeArray[position] = element;
	}

	public void remove(Object k) {
		for (int i = 0; i < current_size; i++) {
			if (nodeArray[i] != null) {
				if (nodeArray[i].getKey().equals(k))
					nodeArray[i] = null;
			}
		}
	}

	private int calculatePosition(Object k, Object v) {

		int index;
		index = k.hashCode() & (current_size - 1);
		return index;
	}

	public Node getElement(int i) {
		return nodeArray[i];
	}

	public Object get(Object k) {

		Object value = null;
		for (int i = 0; i < current_size; i++) {
			if (nodeArray[i] != null) {
				if (nodeArray[i].getKey().equals(k)) {
					value = nodeArray[i].getValue();
					return value;
				}
			}
		}
		return value;
	}

	// HashMap should be increased when current_no_elements becomes large (up to 75% capacity)
	public void increaseHashmap() {
		int newLength = current_size * 2;
		nodeArray = Arrays.copyOf(nodeArray, newLength);
		current_size = newLength;
	}

	public DummyHashMap clone() {
		DummyHashMap newHashMap = new DummyHashMap();
		newHashMap.nodeArray = Arrays.copyOf(nodeArray, current_size);
		newHashMap.current_no_elements = current_no_elements;
		newHashMap.current_size = current_size;

		return newHashMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + DEFAULT_INITIAL_CAPACITY;
		result = prime * result + current_no_elements;
		result = prime * result + current_size;
		result = prime * result + Float.floatToIntBits(load_factor);
		result = prime * result + Arrays.hashCode(nodeArray);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DummyHashMap other = (DummyHashMap) obj;
		if (DEFAULT_INITIAL_CAPACITY != other.DEFAULT_INITIAL_CAPACITY)
			return false;
		if (current_no_elements != other.current_no_elements)
			return false;
		if (current_size != other.current_size)
			return false;
		if (Float.floatToIntBits(load_factor) != Float.floatToIntBits(other.load_factor))
			return false;
		if (!Arrays.equals(nodeArray, other.nodeArray))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DummyHashMap [nodeArray=" + Arrays.toString(nodeArray) + ", DEFAULT_INITIAL_CAPACITY="
				+ DEFAULT_INITIAL_CAPACITY + ", current_no_elements=" + current_no_elements + ", load_factor="
				+ load_factor + ", current_size=" + current_size + "]";
	}

	public boolean isEmpty() {
		if (current_size > 0)
			return true;
		return false;
	}

	public void clear() {
		nodeArray = new Node[DEFAULT_INITIAL_CAPACITY];
		current_size = DEFAULT_INITIAL_CAPACITY;
		current_no_elements = 0;
	}

	public boolean containsKey(Object key) {
		for (int i = 0; i < current_size; i++) {
			if (nodeArray[i] != null) {
				if (nodeArray[i].getKey().equals(key))
					return true;
			}
		}
		return false;
	}

	public boolean containsValue(Object value) {
		for (int i = 0; i < current_size; i++) {
			if (nodeArray[i] != null) {
				if (nodeArray[i].getValue().equals(value))
					return true;
			}
		}
		return false;
	}

	public void printCollection() {
		for (int i = 0; i < current_size; i++) {
			if (nodeArray[i] != null) {
				System.out.println("Klucz: " + nodeArray[i].getKey() + "ma wartosc: " + nodeArray[i].getValue());
			} else {
				System.out.println("Klucz: null ma wartosc: null");
			}
		}
	}

	public static DummyHashMap merge(DummyHashMap dhm1, DummyHashMap dhm2) {
		// check is it null
		if (dhm1.getFirstElement() == null && dhm2.getFirstElement() == null)
			throw new NullPointerException("Cannot merge - both hashmaps are empty or does not exist");
		if (dhm1.getFirstElement() == null)
			return dhm2;
		if (dhm2.getFirstElement() == null)
			return dhm1;

		// check class type equality
		Object firstObjectKeyClass = dhm1.getFirstElement().getKey().getClass();
		Object secondObjectKeyClass = dhm2.getFirstElement().getKey().getClass();
		if (!firstObjectKeyClass.equals(secondObjectKeyClass))
			throw new IllegalArgumentException("Cannot merge - Hashmaps contains different types of key!");
		Object firstObjectValueClass = dhm1.getFirstElement().getValue().getClass();
		Object secondObjectValueClass = dhm2.getFirstElement().getValue().getClass();
		if (!firstObjectValueClass.equals(secondObjectValueClass))
			throw new IllegalArgumentException("Cannot merge - Hashmaps contains different types of key!");

		// check size
		DummyHashMap result = null;
		if (dhm1.current_size >= dhm2.current_size) {
			result = moveElements(dhm2, dhm1);
		} else {
			result = moveElements(dhm1, dhm2);
		}

		return result;
	}

	public int getCurrentSize() {
		return current_size;
	}
	
	public int getCurrentNoElements() {
		return current_no_elements;
	}
	
	public Node[] getNodeArray() {
		return nodeArray;
	}
	
	private static DummyHashMap moveElements(DummyHashMap sourceMap, DummyHashMap destinationMap) {
		for (int i = 0; i < sourceMap.current_size; i++) {
			if (sourceMap.nodeArray[i] != null)
				destinationMap.put(sourceMap.nodeArray[i].getKey(), sourceMap.nodeArray[i].getValue());
		}
		return destinationMap;
	}

	private Node getFirstElement() {
		for (Node i : nodeArray) {
			if (i != null)
				return i;
		}
		return null;
	}
}
